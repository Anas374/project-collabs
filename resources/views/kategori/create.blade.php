@extends('layout.master')

@section('judul')
Halaman Create
@endsection

@section('content')

<form action="/kategori" method="POST">
    @csrf
    <div class="form-group">
      <label>nama kategori</label>
      <input type="text" name="nama" class="form-control"> 
      
    </div>
    <div class="form-group">
      <label>dekripsi</label>
      <textarea name="dekripsi"class="form-control" id="" cols="30"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection