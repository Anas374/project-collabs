<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index');

Route::get('/form','FormController@bio');

Route::post('/welcome','FormController@kirim');

Route::group(['middleware' => ['auth']], function () {
    //CRUD KATEGORI
    Route::get('/kategori/create','kategoricontroller@create');
    Route::post('/kategori','kategoricontroller@store');
    Route::get('/kategori', 'kategoricontroller@index');
    Route::get('/kategori/{kategori_id}', 'kategoricontroller@show');
    Route::get('/kategori/{kategori_id}/edit', 'kategoricontroller@edit');
    Route::put('/kategori/{kategori_id}', 'kategoricontroller@update');
    Route::delete('/kategori/{kategori_id}', 'kategoricontroller@destroy');

    //Update Profile
    Route::resource('profil', 'ProfileController')->only([
        'index', 'update'
    ]);
});




//CRUD BERITA
Route::resource('berita','beritacontroller');


Auth::routes();
