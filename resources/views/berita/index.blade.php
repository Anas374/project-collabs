@extends('layout.master')

@section('judul')
Halaman Berita 
@endsection

@section('content')

@auth
  <a href="/berita/create" class="btn btn-primary my-2">Tambah</a>
@endauth


<div class="row">
   @forelse ($berita as $item)
    
<div class="col-4">
        <div class="card">
            <img src="{{asset('gambar/'.$item->thumbnail)}}" class="card-img-top" alt="...">
            <div class="card-body">
          <h3>{{$item->judul}}</h3>
          <p class="card-text"> {{Str::limit($item->content, 30)}}</p>


          @auth
          <form>
            <form action ="/berita/{{$item->id}}" method="POST">
              @csrf
              @method('delete')
              <a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">detail</a>
            <a href="/berita/{{$item->id}}/edit" class="btn btn-warning btn-sm">edit</a>
            <form action ="/berita/{{$item->id}}" method="POST">
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
            </form>
          @endauth

          @guest
            <a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">detail</a>
          @endguest

          
        </div>
      </div>
      </div>
      @empty
        <h4>Data Berita Belum Ada</h4>
      @endforelse

</div>

@endsection