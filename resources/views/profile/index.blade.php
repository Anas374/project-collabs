@extends('Layout.master')

@section('judul')
Halaman Update Profile
@endsection

@section('content')
<form action="/profil/{{$profile->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Umur</label>
      <input type="text" name="umur" value="{{$kategori->nama}}"class="form-control">
    </div>

    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label>Biodata</label>
      <input type="text" name="bio" class="form-control">{{$kategori->dekripsi}}
    
    </div>

    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror


<div class="form-group">
    <label>Alamat</label>
    <input type="text" name="alamat" class="form-control">{{$kategori->dekripsi}}
  
  </div>

  @error('alamat')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror

    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection