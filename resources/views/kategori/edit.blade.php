@extends('Layout.master')

@section('judul')
Halaman edit kategori {{$kategori->nama}}
@endsection

@section('content')

<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>nama kategori</label>
      <input type="text" name="nama" value="{{$kategori->nama}}"class="form-control">
    </div>

    @error('nama kategori')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label>dekripsi</label>
      <input type="text" name="dekripsi" class="form-control">{{$kategori->dekripsi}}
    
    </div>

    @error('dekripsi')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection